using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    float lenght, starpost;
    public GameObject cam;
    public float parala;
    // Start is called before the first frame update
    void Start()
    {
        starpost = transform.position.x;
        lenght = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float temp = (cam.transform.position.x * (1 - parala));
        float dist = (cam.transform.position.x * parala);
        transform.position = new Vector3(starpost + dist, transform.position.x, transform.position.z);
        if (temp > starpost + lenght) starpost += lenght;
        else if (temp < starpost - lenght) starpost -= lenght;
    }
}
