using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puntos : MonoBehaviour
{
    public AudioClip clip;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {

            if(clip == true)
            {
                Destroy(this.gameObject);
                AudioManager.Instance.ReproducirAudio(clip);
            }
        }
    }
}
