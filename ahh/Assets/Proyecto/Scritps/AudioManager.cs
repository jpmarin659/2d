using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }
    public AudioSource source;
    // Start is called before the first frame update
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Suena");
        }
    }
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void ReproducirAudio(AudioClip audio)
    {
        source.PlayOneShot(audio);
    }
}
