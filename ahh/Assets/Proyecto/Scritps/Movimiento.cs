using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float speed;
    public float jump;
    Rigidbody2D rb;
    public SpriteRenderer render;
    public Animator anim;
    private ChekPoint chek;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {

    }
    private void FixedUpdate()
    {
        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            render.flipX = false;
            anim.SetBool("Run", true);
        }
        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            render.flipX = true;
            anim.SetBool("Run", true);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            anim.SetBool("Run", false);
        }
        if(Input.GetKey("space") && Salto.isGround)
        {
            rb.velocity = new Vector2(rb.velocity.x, jump );
            anim.SetBool("Jump", true);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Died")
        {
            transform.position = chek.che;
        }
        if (collision.tag == "Point")
        {
            chek = collision.GetComponent<ChekPoint>();
            chek.gameObject.SetActive(false);
        }
    }
}
